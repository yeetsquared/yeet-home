module Yeet exposing (..)

import Element exposing (rgb255)
import Element.Border as Border
import Html exposing (Html)
import Bitwise
import Svg
import Svg.Attributes
import Color
import Collage exposing (Point)
import Element exposing (Element, paddingEach)
import Collage exposing (polygon, filled, uniform)
import Collage.Render exposing (svg)
import Element exposing (Attr)
import Element exposing (centerY)

debug : Bool
debug = False
borderDebug : Int -> List (Attr () msg)
borderDebug n = if not debug then [] else
  [ Border.color (hex n)
  , Border.width 1
  , Border.dashed ]

ghex : (Int -> Int -> Int -> color) -> Int -> color
ghex frgb n = let r = n |> Bitwise.shiftRightBy 16 |> Bitwise.and 0xff
                  g = n |> Bitwise.shiftRightBy 8  |> Bitwise.and 0xff
                  b = n |> Bitwise.and 0xff
              in frgb r g b

hex : Int -> Element.Color
hex  = ghex rgb255
chex : Int -> Color.Color
chex = ghex Color.rgb255

svgYeet : Int -> List Point -> Element msg
svgYeet = svgPolygon 10
svgPolygon : Float -> Int -> List Point -> Element msg
svgPolygon x h p =
  -- I don't know why but somehow there's a 6px whitespace below the svg everytime
  let truePadding n = paddingEach { top = n, right = n, left = n, bottom = n - 6 }
  in List.map (mulTup x) p
    |> polygon |> filled (uniform <| chex h)
    |> svg |> Element.html
    |> Element.el (borderDebug 0x3280ff
                  ++ [ centerY, truePadding 15 ])

mulTup : Float -> Point -> Point
mulTup x (a, b) = (a * x, b * x)



yeetLambdaPath          : List Point
yeetLambdaPath          = [ (0, 23), (7, 23), (21, 0), (14, 0), (10.5, 5.75), (7, 0), (0, 0), (7, 11.5) ]
yeetBracketRightPath    : List Point
yeetBracketRightPath    = [ (0, 23), (14, 23), (14, 14.5), (21, 14.5), (21, 8.5)
                          , (14, 8.5), (14, 0), (0, 0), (0, 6), (8, 6), (8, 17), (0, 17) ]
yeetBracketLeftPath     : List Point
yeetBracketLeftPath     = List.map (\(x, y) -> (21 - x, y)) yeetBracketRightPath
yeetTPath               : List Point
yeetTPath               = [ (0, 23), (21, 23), (21, 17), (13.5, 17)
                          , (13.5, 0), (7.5, 0), (7.5, 17), (0, 17) ]
squaredBracketLeftPath  : List Point
squaredBracketLeftPath  = [ (0, 62), (12, 62), (12, 57), (5, 57), (5, 5), (12, 5), (12, 0), (0, 0) ]
brokenBracketRightPath  : List Point
brokenBracketRightPath  = [ (7, 47), (12, 47), (12, 0), (0, 0), (0, 5), (7, 5) ]
squaredRedPath          : List Point
squaredRedPath          = [ (0, 12), (12, 12), (12, 5), (18, 5), (18, 0), (7, 0), (7, 7), (0, 7) ]