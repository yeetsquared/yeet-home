module Main exposing (..)

import Browser
import Html exposing (Html)
import Html.Attributes as Attr
import Element exposing (..)
import Element.Background as Background
import Element.Events as Events
import Collage exposing (Point)
import Yeet exposing (..)


main : Program () YeetModel Msg
main = Browser.sandbox  { init = init
                        , view = view
                        , update = update }



type alias YeetPath = { path : List Point
                      , hovered : Bool }
yeetPath : List Point -> YeetPath
yeetPath p  = { path = p
              , hovered = False }

type alias Matrix22 a = ( ( a, a ), ( a, a ) )
type alias YeetModel  = { yeet : Matrix22 YeetPath
                        , brackets :
                            { paths : ( List Point, List Point )
                            , hovered : Bool }
                        , squared : YeetPath }

yeetMap : (a -> b) -> Matrix22 a -> Matrix22 b
yeetMap f ( ( ynw, yne ), ( ysw, yse ) ) = ( ( f ynw, f yne ), ( f ysw, f yse ) )

yeetZip : (a -> b -> c) -> Matrix22 a -> Matrix22 b -> Matrix22 c
yeetZip f ( ( anw, ane ), ( asw, ase ) ) ( ( bnw, bne ), ( bsw, bse ) ) =
  ( ( f anw bnw, f ane bne ), ( f asw bsw, f ase bse ) )


init  : YeetModel
init  = 
  { yeet =
    ( ( yeetLambdaPath, yeetBracketRightPath )
    , ( yeetBracketLeftPath, yeetTPath ) ) |> yeetMap yeetPath
  , brackets =
    { paths = ( squaredBracketLeftPath, brokenBracketRightPath )
    , hovered = False }
  , squared = yeetPath squaredRedPath }



view : YeetModel -> Html Msg
view model =
  let ( ( sypNW, sypNE ), ( sypSW, sypSE ) ) = model.yeet
      localRow ps s = row (borderDebug 0xff0000 ++ ps
                 ++ [ centerX, centerY, spacing s ])
      localColumn ps = column (ps ++ [ height fill, centerX, smoothStyle ])

      logo = localRow [ Events.onMouseEnter <| BracketHover True
                      , Events.onMouseLeave <| BracketHover False ] -80
            [ svgYeet 0x505050 squaredBracketLeftPath
              |> Element.el (ypbracketKit moveLeft model.brackets.hovered)
            , localColumn []
                (List.map (localRow [] 0)
                  [ [ ypsvg 0x699642 yeetHoverNW sypNW, ypsvg 0x699642 yeetHoverNE sypNE ]
                  , [ ypsvg 0x699642 yeetHoverSW sypSW, ypsvg 0x699642 yeetHoverSE sypSE ] ])
            , localColumn (ypbracketKit moveRight model.brackets.hovered)
                [ ypsvg 0xdc2820 SquaredHover model.squared
                , svgYeet 0x505050 brokenBracketRightPath ] ]
            |> Element.el (borderDebug 0xff00ff ++ [ width fill, height fill ])
  
  in column (borderDebug 0xffff00 ++ [ width fill, height fill ])
      [ logo ]
    |> layout [ Background.color (hex 0x242424) ]



type Msg = YeetHovers (Matrix22 Bool)
         | BracketHover Bool
         | SquaredHover Bool
-- ( ( bnw, bne ), ( bsw, bse ) )
update : Msg -> YeetModel -> YeetModel
update msg model =
  case msg of
    YeetHovers yeetb ->
      { model | yeet = yeetZip (\y b -> { y | hovered = b }) model.yeet yeetb }
    BracketHover b ->
      let brackets = model.brackets
      in { model | brackets = { brackets | hovered = b } }
    SquaredHover b ->
      let squared = model.squared
      in { model | squared = { squared | hovered = b } }


yeetHoverNW : Bool -> Msg
yeetHoverNW b = YeetHovers ( ( b, False ), ( False, False ) )
yeetHoverNE : Bool -> Msg
yeetHoverNE b = YeetHovers ( ( False, b ), ( False, False ) )
yeetHoverSW : Bool -> Msg
yeetHoverSW b = YeetHovers ( ( False, False ), ( b, False ) )
yeetHoverSE : Bool -> Msg
yeetHoverSE b = YeetHovers ( ( False, False ), ( False, b ) )


ypbracketKit : (Float -> Attribute Msg) -> Bool -> List (Attribute Msg)
ypbracketKit movef b = [ movef <| if b then 80 else 0, smoothStyle ]

ypsvg : Int -> (Bool -> Msg) -> YeetPath -> Element Msg
ypsvg c yeetf syp = syp.path |> svgYeet c |> Element.el
      [ moveUp <| if syp.hovered then 10 else 0
      , filterStyle <| "brightness(" ++ if syp.hovered then "2" else "1" ++ ")"
      --, filterStyle <| "grayscale(" ++ if syp.hovered then "60%" else "0%" ++ ")"
      , Events.onMouseEnter <| yeetf True
      , Events.onMouseLeave <| yeetf False
      , smoothStyle ]

smoothStyle : Attribute msg
smoothStyle = Element.htmlAttribute <| Attr.style "transition" "0.2s ease-out"

filterStyle : String -> Attribute msg
filterStyle = Element.htmlAttribute << Attr.style "filter"